FROM rancher/server:stable

ENV LD_LIBRARY_PATH="/openssl-1.1.1h:${LD_LIBRARY_PATH}"

RUN apt-get update && apt-get install -y \
        wget \
        build-essential \
        checkinstall \
        --no-install-recommends \
    && rm -rf /var/lib/apt/lists/*

RUN wget https://ftp.openssl.org/source/openssl-1.1.1h.tar.gz \
    && tar -zxf openssl-1.1.1h.tar.gz \
    && cd openssl-1.1.1h \
    && ./config \
    && make \
    && make install

COPY cert.crt/ENV_DB_CERT /var/lib/rancher/etc/ssl/ca.crt
